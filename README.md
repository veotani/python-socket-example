# Requirements

1. pipenv

# Setup project

Run in current directory:

```
pipenv install
```

# Configuration

Change host and port in .env if necessary

# Start server

Run in current directory:

```
pipenv run python server.py
```

# Start client

When server is on run in current directory:

```
pipenv run python client.py
```